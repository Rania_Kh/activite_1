#!/usr/bin/env python
import rospy
from geometry_msgs.msg import Twist
from sensor_msgs.msg import Joy
​
def callback(data):
    twist = Twist()  #Créer un message Twist
    twist.linear.x = 4*data.axes[7]    #ajouter des valeurs pour linear x et angular z a partir de joy
    twist.angular.z = 4*data.axes[6]
    pub.publish(twist) # publier le msg 
​
def init():
    global pub
    pub = rospy.Publisher('turtle1/cmd_vel', Twist, queue_size=1) #definir turtle1/cmd_vel en tant qu'un publisher
    rospy.Subscriber("joy", Joy, callback) #joy en mode écouteur
    rospy.init_node('JoyTurtle') #intialisation de noeud joy
    rospy.spin() #mise en écoute
​
if __name__ == '__main__':
    init() #appel au fct init()