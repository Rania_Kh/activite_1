Configuration de Turtlesim avec une manette PS4
=====================
Dans cette tâche , on a voulu tester le fonctionnement d'un turtlesim avec une manette PS4


Démarrage
============

>#### 1. Pour installer Joystick package :

                 $ sudo apt-get install ros-indigo-joy 

>#### 2.  Pour configurer the joystick : </br>

                 $ sudo jstest /dev/input/jsX 
>
>#### 3. fonctionner the joy node :</br>


                  $ roscore
                  $ rosparam set joy_node/dev "/dev/input/jsX"
                  $ rosrun joy joy_node 

>#### 4. Lancer le script python:</br>

                  $ python control.py </br>

Environnements de travail
==========================
OS : Ubuntu 14.04 Trusty ROS: Indigo