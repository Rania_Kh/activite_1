#!/usr/bin/env python
# -*-coding:Latin-1 -*
import unittest 
import os
import sys
sys.path.append(os.path.abspath('../src'))
from patrol import Act_2

test_act2= Act_2()

# Classe de test 
class Test_distance (unittest.TestCase):

    
    
    #test nominal pour tester un processus nominal
    def test_nominal(self):
        self.path_valid="config.yaml"
        self.assertRaises(TypeError,test_act2.verif_path(self.path_valid))
        print("Test nominal Reussi ")

  
    #test degradé pour  tester un processus erroné
    
    def test_degrade(self):
        self.path_invalid=""
        self.assertRaises(TypeError,test_act2.verif_path(self.path_invalid))
       
if __name__ == "__main__":
       unittest.main()

