#!/usr/bin/env python
# -*-coding:Latin-1 -*
import os
import sys
sys.path.append(os.path.abspath('../src'))
from fixedVelocity import fixe
import unittest

test_a= fixe()
# Classe de test 
class Test(unittest.TestCase):

 
   

   #test nominal pour tester un processus nominal
   def test_nominal(self):
       expected_value=10
       self.assertEqual(test_a.param_val(),expected_value,msg="OK")
       print("\n") 
       print ("test Nominal------> OK")
       print("\n") 

  #test degradé pour  tester un processus erroné
   def test_degrade(self):
      unexpected_value=0
      self.assertEqual(test_a.param_val(),unexpected_value)
       



if __name__ == "__main__":
       unittest.main()

