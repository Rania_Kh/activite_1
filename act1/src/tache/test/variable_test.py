#!/usr/bin/env python
# -*-coding:Latin-1 -*
import os
import sys
sys.path.append(os.path.abspath('../src'))
from variableVelocity import variable
import unittest

test_b= variable()
# Classe de test 
class Test(unittest.TestCase):

 
   

   #test nominal pour tester un processus nominal
   def test_nominal(self):
       expected_value=30
       self.assertEqual(test_b.param_Maxval(),expected_value,msg="OK")
       print("\n") 
       print ("test Nominal------> OK")
       print("\n") 
   #test degradé pour  tester un processus erroné
   def test_degrade(self):
       unexpected_value=0
       self.assertEqual(test_b.param_Maxval(),unexpected_value,msg="Error")
       



if __name__ == "__main__":
       unittest.main()

