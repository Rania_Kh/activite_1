tache
=====================
Turtlesim est configuré par une vitesse fixe
La vitesse est fixée dans un fichier config/vitesse.yaml cette valeur sera chargé dans le fichier **launch/variableVelocity.launc**h qui sera par la suite utiliser par un script python **scr/variableVelocity.py**

Démarrage
============
ouvrir 2 terminales :

#### 1. terminale 1 :

          $ roscore
#### 2. terminale 2 :
          $ cd ~/act1
          $ catkin_make
          $ source devel/setup.bash
          $ cd /src/tache
          $ roslaunch tache variableVelocity.launch
          $ rosrun tache variableVelocity.py
